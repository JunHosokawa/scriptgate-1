unit SG.ScriptGateHelper;

interface

procedure LogD(const iMsg: String);

implementation

{$IFDEF DEBUG}
uses
  FMX.Log;
{$ENDIF}

procedure LogD(const iMsg: String);
begin
{$IFDEF DEBUG}
  Log.d(iMsg);
{$ENDIF}
end;

end.